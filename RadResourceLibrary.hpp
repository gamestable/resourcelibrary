/*
 * RadResourceLibrary.hpp
 *
 *  Created on: Apr 24, 2017
 *      Author: games-table.studio - Steffen Kremer
 */

#ifndef SRC_INCLUDE_RADRESOURCELIBRARY_HPP_
#define SRC_INCLUDE_RADRESOURCELIBRARY_HPP_

//For use:
//Link to zdll.lib BEFORE RadResource!!!
//Copy zlib1.dll to exe location

#include <iostream>
#include <fstream>

const char RADRESOURCELIBRARY_VERSION = 4;

namespace rad
{
namespace resourcelibrary
{

struct ContainerHeader
{
	unsigned char magic1 = 'R';
	unsigned char magic2 = 'D';
	unsigned char version = RADRESOURCELIBRARY_VERSION;
	unsigned int fileCount = 0;
	unsigned int fileSize = 0; //must change datatype size in source
};

const unsigned int RADRESOURCECONTAINERHEADER_SIZE = 11;

struct ResourceHeader
{
	unsigned int id = 0;
	unsigned int size = 0;
	unsigned int uncompressedSize = 0;
	unsigned char sizeName = 0;
	//name in char with sizename length
	unsigned char stat = 0; //1=compressed(zlib)
	unsigned char type = 0;
	unsigned int crc32 = 0;
};

const unsigned int RADRESOURCEHEADER_SIZE = 20; //with null from name string + name char array size

class CResource
{
public:
	CResource() {};
	~CResource();

	unsigned int GetID() {return m_id;};
	//Return Resource ID in Container
	unsigned int GetSize() {return m_size;};
	//Return size in bytes of data
	unsigned int GetUncompSize() {return m_uncompressedSize;};
	//Return compressed size in bytes (=getSize if uncompressed)
	std::string GetName() {return m_name;};
	//Returns File/resource name
	unsigned char GetStat() {return m_stat;};
	//Returns stat flags
	bool IsCompressed() {return (m_stat==1);};
	//Return true if data is compressed
	//bool IsEncrypted();
	//Return true if data is encrypted
	unsigned char GetType() {return m_type;};
	//Get media type (media type=file endings see below) (uninteresting if not used with RadEngine)
	unsigned int GetCRC32() {return m_crc32;};
	//Get data CRC32 (calculated with intern crc32!)

//private:
	void * m_data = nullptr;
	unsigned int m_id = 0;
	unsigned int m_size = 0;
	unsigned int m_uncompressedSize = 0;
	unsigned char m_stat = 0; //1=compressed(zlib) //
	unsigned char m_type = 0; //Defined by file ending in use with radengine
	/*
		* other=0
		* texture=1
		* textureinfo=2
		* sound=3
		* soundinfo=4
		* animation=5
		* animationinfo=6
		* shader vert=7
		* shader frag=8
		* TrueTypeFont=9
		* LUA=10
		* Binary LUA=11
	*/
	unsigned int m_crc32 = 0;
	bool m_crc32correct = false;
	std::string m_name = "";
};

class CContainer
{
public:
	CContainer(std::string file);
	//Open/create container file direct on construct
	CContainer() {};
	~CContainer();

	bool Open(std::string);
	//open/create Container
	void Close();
	//close Container
	bool IsOpen();
	//Returns true if container is properly open and valid (header)

	unsigned int GetFileCount();
	//Return total file count in Container
	unsigned char GetVersion();
	//Return Version of containers RRL version
	std::string GetName();
	//Return Container file name (without path)
	std::string GetFileName();
	//Return Container file name

	bool Load(unsigned int id, CResource * data, bool loadData);
	//Extract data in memory (like out of a zip file) selected by id (you should use this) or name
	//Set loadData to false to just get infos about The Resource like id size and stats and not the actual data

	unsigned int FindIDbyName(std::string);
	//Find resource id by resource (file) name

	//bool ExtractToFile(unsigned int id, std::string file);
	//Extract data to file (like out of a zip file) selected by id (you should use this) or name

	bool AddFile(std::string file, bool compress);
	//Add file to container
	bool AddFolder(std::string folder, bool compress, bool recursive);
	//Add folder to container
	//Recursive = include subdirectories

	bool CheckHeader();
	//Moustly used internal, checks for valid header

	void SetSTDOUT(bool enable) {m_enableStdout=enable;};
	//Enable if you want std::cout feedback

private:
	std::string m_file = "";
	std::fstream m_fileObj;
	ContainerHeader m_header;
	bool m_enableStdout = false;
};

}
}

#endif /* SRC_INCLUDE_RADRESOURCELIBRARY_HPP_ */
