/*
 * CResourceLibrary.cpp
 *
 *  Created on: Apr 24, 2017
 *      Author: games-table.studio - Steffen Kremer
 */

#include "RadResourceLibrary.hpp"
#include <fstream>
#include <vector>
#include <iostream>
#include <algorithm>
#include "zlib.h"
#include "dirent.h"
#include "windows.h"
#include "crc32.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>

namespace rad
{
namespace resourcelibrary
{

bool dirExists(const std::string& dirName_in)
{
	DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;  //something is wrong with your path!

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;   // this is a directory!
	return false;    // this is not a directory!
}

struct MatchPathSeparator
{
    bool operator()( char ch ) const
    {
        return ch == '\\';
    }
};

std::string basename( std::string const& pathname )
{
    return std::string(std::find_if( pathname.rbegin(), pathname.rend(),MatchPathSeparator()).base(),pathname.end());
}

std::string getFileExtension(std::string const& fileName)
{
	return fileName.substr(fileName.find_last_of(".") + 1);
}

//***************RRC Class*****************

CContainer::CContainer(std::string file)
{
	Open(file);
}

CContainer::~CContainer()
{
	Close();
}

bool CContainer::Open(std::string file)
{
	Close();
	m_fileObj.open(file.c_str(),std::ios::in | std::ios::out | std::ios::ate | std::ios::binary);
	m_fileObj.seekg(0,std::ios::beg);
	m_fileObj.seekp(0,std::ios::beg);
	if (m_fileObj)
	{
		//Check Header
		ContainerHeader m_headerCompare;
		m_fileObj.read(reinterpret_cast<char*>(&m_header.magic1), 1);
		m_fileObj.read(reinterpret_cast<char*>(&m_header.magic2), 1);
		m_fileObj.read(reinterpret_cast<char*>(&m_header.version), 1);
		m_fileObj.read(reinterpret_cast<char*>(&m_header.fileCount), sizeof(unsigned int));
		m_fileObj.read(reinterpret_cast<char*>(&m_header.fileSize), sizeof(unsigned int));
		if (!(m_header.magic1==m_headerCompare.magic1&&m_header.magic2==m_headerCompare.magic2&&m_header.version==m_headerCompare.version))
		{
if (m_enableStdout) {
			std::cout << "Container is damaged or wrong version!" << std::endl;;
}
			m_fileObj.close();
			return false;
		}
		else
		{
if (m_enableStdout) {
			std::cout << "Container opened!" << std::endl;;
}
			m_file = file;
			return true;
		}
	}
	else
	{
		//Create and write header
if (m_enableStdout) {
		std::cout << "Container doesnt exist, create new one." << std::endl;;
}
		m_fileObj.open(file.c_str(),std::ios::in | std::ios::out | std::ios::trunc | std::ios::binary);
		if (m_fileObj.is_open())
		{
			{
				ContainerHeader tmp;
				m_header = tmp;
			}
			m_fileObj.seekg(0,std::ios::beg);
			m_fileObj.seekp(0,std::ios::beg);
			m_fileObj.write(reinterpret_cast<char*>(&m_header.magic1), 1);
			m_fileObj.write(reinterpret_cast<char*>(&m_header.magic2), 1);
			m_fileObj.write(reinterpret_cast<char*>(&m_header.version), 1);
			m_fileObj.write(reinterpret_cast<char*>(&m_header.fileCount), sizeof(unsigned int));
			m_fileObj.write(reinterpret_cast<char*>(&m_header.fileSize), sizeof(unsigned int));
			m_fileObj.flush();
			m_file = file;
if (m_enableStdout) {
			std::cout << "Container created and opened!" << std::endl;;
}
			return true;
		}
		else
		{
if (m_enableStdout) {
			std::cout << "Cant create new Container! Code: " << strerror(errno) << std::endl;;
}
			return false;
		}
	}
	return false;
}

void CContainer::Close()
{
	if (m_fileObj.is_open())
	{
		m_fileObj.seekg(0,std::ios::beg);
		m_fileObj.seekp(0,std::ios::beg);
		m_fileObj.clear();
		m_fileObj.flush();
		m_fileObj.close();
	}
	m_file = "";
	{
		ContainerHeader tmp;
		m_header = tmp;
	}
}

bool CContainer::IsOpen()
{
	return m_fileObj.is_open() && m_file!="";// && m_fileObj;
}

unsigned int CContainer::GetFileCount()
{
	return m_header.fileCount;
}

unsigned char CContainer::GetVersion()
{
	return m_header.version;
}

std::string CContainer::GetName()
{
	return basename(m_file);
}

std::string CContainer::GetFileName()
{
	return m_file;
}

bool CContainer::AddFile(std::string file, bool compressFile)
{
	std::string fileAddName = basename(file);
	if (fileAddName.length()>255)
	{
if (m_enableStdout) {
		std::cout << "File name exceeding 255 chars!" << std::endl;
}
		return false;
	}
	ContainerHeader m_headerCompare;
if (m_enableStdout) {
	std::cout << "Start adding: " << file << std::endl;
	std::cout << "Stat: Compressed=" << compressFile << std::endl;
}
	if (!CheckHeader())
	{
		return false;
	}
	m_fileObj.clear();
	std::fstream fileAdd(file.c_str(),std::ios::in|std::ios::binary); //std::ios::out|
	if (!fileAdd.is_open())
	{
if (m_enableStdout) {
		std::cout << "Cant open file!" << std::endl;
}
		return false;
	}
	//Get file size and check if alloc possible
	fileAdd.seekg(0,std::ios::end);
	unsigned int fileAddSize = fileAdd.tellg();
	//!!!!NEED TO HANDLE INT OVERFLOW!!!!
	fileAdd.seekg(0,std::ios::beg);
	unsigned char * buffer = new(std::nothrow) unsigned char[fileAddSize];
	unsigned char * bufferZ = nullptr;
	unsigned int bufferZSize = compressBound(fileAddSize)*1;
	unsigned int origFileAddSize = fileAddSize;
	if (compressFile)
	{
		bufferZ = new(std::nothrow) unsigned char[bufferZSize];
	}
	if (!bufferZ&&compressFile)
	{
if (m_enableStdout) {
		std::cout << "Buffer Error!" << std::endl;
}
		return false;
	}
	if (!buffer)
	{
if (m_enableStdout) {
		std::cout << "Buffer Error!" << std::endl;
}
		return false;
	}
	fileAdd.read((char *)buffer,fileAddSize);
	if (compressFile)
	{
		long unsigned int newFileAddSize = bufferZSize;
		if (compress(bufferZ, &newFileAddSize, buffer, fileAddSize)!=Z_OK)
		{
if (m_enableStdout) {
			std::cout << "Compress Error!" << std::endl;
}
			return false;
		}
		fileAddSize = (unsigned long) newFileAddSize;
if (m_enableStdout) {
		std::cout << "Compressed Size:" << fileAddSize << std::endl;
}
	}
	//Rewrite Header
	m_fileObj.seekg(0,std::ios::beg);
	m_fileObj.seekp(0,std::ios::beg);
	m_header.fileCount++;
	m_header.fileSize += fileAddSize;
	m_fileObj.write(reinterpret_cast<char*>(&m_header.magic1), sizeof(unsigned char));
	m_fileObj.write(reinterpret_cast<char*>(&m_header.magic2), sizeof(unsigned char));
	m_fileObj.write(reinterpret_cast<char*>(&m_header.version), sizeof(unsigned char));
	m_fileObj.write(reinterpret_cast<char*>(&m_header.fileCount), sizeof(unsigned int));
	m_fileObj.write(reinterpret_cast<char*>(&m_header.fileSize), sizeof(unsigned int));
	m_fileObj.flush();

	//Prepare res entry
	ResourceHeader m_headerRH;
	m_headerRH.id = m_header.fileCount;
	m_headerRH.sizeName = fileAddName.length();
	m_headerRH.size = fileAddSize;
	//TODO: Add more fileextensions
	if (getFileExtension(fileAddName)=="png"||getFileExtension(fileAddName)=="tga"||getFileExtension(fileAddName)=="jpg")
	{
		m_headerRH.type = 1;
	}
	else if (getFileExtension(fileAddName)=="mp3"||getFileExtension(fileAddName)=="wav"||getFileExtension(fileAddName)=="ogg")
	{
		m_headerRH.type = 3;
	}
	else if (getFileExtension(fileAddName)=="anm")
	{
		m_headerRH.type = 5;
	}
	else if (getFileExtension(fileAddName)=="vert")
	{
		m_headerRH.type = 7;
	}
	else if (getFileExtension(fileAddName)=="frag")
	{
		m_headerRH.type = 8;
	}
	else if (getFileExtension(fileAddName)=="ttf")
	{
		m_headerRH.type = 9;
	}
	else if (getFileExtension(fileAddName)=="lua")
	{
		m_headerRH.type = 10;
	}
	else if (getFileExtension(fileAddName)=="blua")
	{
		m_headerRH.type = 11;
	}
	m_headerRH.stat = (char)compressFile;
	m_headerRH.uncompressedSize = origFileAddSize;
	if (compressFile)
	{
		m_headerRH.crc32 = crc32(bufferZ,fileAddSize);
	}
	else
	{
		m_headerRH.crc32 = crc32(buffer,fileAddSize);
	}
if (m_enableStdout) {
	if (!compressFile)
	{
		std::cout << "Size: " << m_headerRH.size << std::endl;
	}
	std::cout << "Name: " << fileAddName << std::endl;
	std::cout << "ID: " << m_headerRH.id << std::endl;
	std::cout << "Stat: " << (unsigned int)m_headerRH.stat << std::endl;
	std::cout << "Type: " << (unsigned int)m_headerRH.type << std::endl;
	std::cout << "CRC32: " << m_headerRH.crc32 << std::endl;
}

	m_fileObj.seekg(0,std::ios::end);
	m_fileObj.seekp(0,std::ios::end);
	m_fileObj.write(reinterpret_cast<char*>(&m_headerRH.id), sizeof(m_headerRH.id));
	m_fileObj.write(reinterpret_cast<char*>(&m_headerRH.size), sizeof(m_headerRH.size));
	m_fileObj.write(reinterpret_cast<char*>(&m_headerRH.uncompressedSize), sizeof(m_headerRH.uncompressedSize));
	m_fileObj.write(reinterpret_cast<char*>(&m_headerRH.sizeName), sizeof(m_headerRH.sizeName));
	m_fileObj.flush();
	for (unsigned char i=0;i<fileAddName.length();i++)
	{
		m_fileObj << fileAddName.at(i);
	}
	char t = 0;
	m_fileObj.write(&t,sizeof(char));
	m_fileObj.write(reinterpret_cast<char*>(&m_headerRH.stat), sizeof(m_headerRH.stat));
	m_fileObj.write(reinterpret_cast<char*>(&m_headerRH.type), sizeof(m_headerRH.type));
	m_fileObj.write(reinterpret_cast<char*>(&m_headerRH.crc32), sizeof(m_headerRH.crc32));
	m_fileObj.flush();

	//Write m_data behind entry
	m_fileObj.flush();
	m_fileObj.seekg(0,std::ios::end);
	m_fileObj.seekp(0,std::ios::end);
	if (compressFile)
	{
		m_fileObj.write((char *)bufferZ,fileAddSize);
	}
	else
	{
		m_fileObj.write((char *)buffer,fileAddSize);
	}
	m_fileObj.flush();
	delete[] buffer;
	if (compressFile)
	{
		delete[] bufferZ;
	}
	fileAdd.close();
	return true;
}

bool CContainer::AddFolder(std::string folder, bool compressFile, bool recursive)
{
	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir(folder.c_str())) != NULL)
	{
if (m_enableStdout) {
		std::cout << "Start adding folder: " << folder << std::endl;
}
		unsigned int files = 0;
		while((ent = readdir(dir)) != NULL)
		{
			if (std::string(".").compare(ent->d_name))
			{
				if (std::string("..").compare(ent->d_name))
				{
					std::string nFile = folder+"\\"+std::string(ent->d_name);
					if (!dirExists(nFile))
					{
						if (!AddFile(nFile,compressFile))
						{
if (m_enableStdout) {
							std::cout << "Error adding File!" << std::endl << "Added " << files << " file(s)!" << std::endl;
}
							return false;
						}
						files++;
					}
				}
			}
		}
if (m_enableStdout) {
		std::cout << "Added " << files << " file(s)!" << std::endl;
}
		closedir(dir);
	}
	return true;
}

bool CContainer::Load(unsigned int id, CResource * data, bool loadData)
{
	if (!CheckHeader()||id==0||data==nullptr)
	{
		return false;
	}
	//Calculate pos for new entry
	//unsigned int fileAddStartPos = RADRESOURCECONTAINERHEADER_SIZE;//sizeof(ContainerHeader);
	m_fileObj.seekg(RADRESOURCECONTAINERHEADER_SIZE,std::ios::beg);
	m_fileObj.seekp(RADRESOURCECONTAINERHEADER_SIZE,std::ios::beg);
	if (m_header.fileCount>0)
	{
		unsigned int completeNameLen = 0;
		unsigned int completeSizeLen = 0;
		for(unsigned int ccc=0;ccc<m_header.fileCount;ccc++)
		{
			m_fileObj.seekg(RADRESOURCECONTAINERHEADER_SIZE+(RADRESOURCEHEADER_SIZE*ccc)+completeNameLen+completeSizeLen,std::ios::beg);
			m_fileObj.seekp(RADRESOURCECONTAINERHEADER_SIZE+(RADRESOURCEHEADER_SIZE*ccc)+completeNameLen+completeSizeLen,std::ios::beg);
			ResourceHeader m_headerSearch;
			if (m_fileObj.eof())
			{
				break;
			}
			//std::cout << "NewP1: " << (unsigned int)m_fileObj.tellg() << "/" << sizeof(m_headerSearch.id) << std::endl;
			m_fileObj.read(reinterpret_cast<char*>(&m_headerSearch.id), sizeof(m_headerSearch.id));
			m_fileObj.read(reinterpret_cast<char*>(&m_headerSearch.size), sizeof(m_headerSearch.size));
			m_fileObj.read(reinterpret_cast<char*>(&m_headerSearch.uncompressedSize), sizeof(m_headerSearch.uncompressedSize));
			m_fileObj.read(reinterpret_cast<char*>(&m_headerSearch.sizeName), sizeof(m_headerSearch.sizeName));
			std::string name(m_headerSearch.sizeName,'\0');
			completeNameLen += m_headerSearch.sizeName;
			for (unsigned char i=0;i<m_headerSearch.sizeName;i++)
			{
				m_fileObj >> name.at(i);
			}
			m_fileObj.seekg(1,std::ios::cur); //(unsigned int)m_fileObj.tellg()+
			m_fileObj.read(reinterpret_cast<char*>(&m_headerSearch.stat), sizeof(m_headerSearch.stat));
			m_fileObj.read(reinterpret_cast<char*>(&m_headerSearch.type), sizeof(m_headerSearch.type));
			m_fileObj.read(reinterpret_cast<char*>(&m_headerSearch.crc32), sizeof(m_headerSearch.crc32));
			//fileAddStartPos = (unsigned int)m_fileObj.tellg();
if (m_enableStdout) {
			//std::cout << "NewP: " << (unsigned int)m_fileObj.tellg() << "/" << fileAddStartPos << std::endl;
			std::cout << "Name: " << name << std::endl;
			std::cout << "Size: " << m_headerSearch.size << " // " << m_headerSearch.uncompressedSize << std::endl;
			std::cout << "ID: " << m_headerSearch.id << std::endl;
			std::cout << "Stat: " << (unsigned int)m_headerSearch.stat << std::endl;
			std::cout << "Type: " << (unsigned int)m_headerSearch.type << std::endl;
			std::cout << "CRC: " << m_headerSearch.crc32 << std::endl;
			std::flush(std::cout);
}
			completeSizeLen += m_headerSearch.size;
			if (m_headerSearch.id==id)
			{
				data->m_crc32 = m_headerSearch.crc32;
				data->m_id = m_headerSearch.id;
				data->m_name = name;
				data->m_size = m_headerSearch.size;
				data->m_uncompressedSize = m_headerSearch.size;
				data->m_stat = m_headerSearch.stat;
				data->m_type = m_headerSearch.type;
				unsigned char * buffer = new(std::nothrow) unsigned char[m_headerSearch.size];
				if (buffer==nullptr)
				{
					return false;
				}
				m_fileObj.read((char *)buffer,m_headerSearch.size);
				unsigned char * bufferZ = nullptr;
				if (data->m_data!=nullptr)
				{
					delete (char*)data->m_data; //Ignore because void pointer is raw data (char=1byte)
				}
				data->m_data = buffer;
				if (data->m_stat)
				{
					bufferZ = new(std::nothrow) unsigned char[m_headerSearch.uncompressedSize];
					data->m_crc32correct = (crc32(buffer,m_headerSearch.size) == data->m_crc32);
					long unsigned int uncomSiz = m_headerSearch.uncompressedSize;
					long unsigned int siz = m_headerSearch.size;
					if (uncompress(bufferZ, &uncomSiz, buffer, siz)!=Z_OK)
					{
						return false;
					}
					data->m_uncompressedSize = (unsigned int)uncomSiz;
					data->m_data = bufferZ;
					delete[] buffer;
					return true;
				}
				data->m_crc32correct = (crc32(buffer,m_headerSearch.size) == data->m_crc32);
				return true;
			}
			else
			{
				if (m_enableStdout) {
			std::cout << "Wrong ID, searching... " << std::endl;
				}
			}

			m_fileObj.seekg(m_headerSearch.size,std::ios::cur);
			m_fileObj.seekp(m_headerSearch.size,std::ios::cur);
		}
	}
	else
	{
if (m_enableStdout) {
		std::cout << "Container is empty. FC:" << m_header.fileCount << " FS:" << m_header.fileSize << std::endl;
}
	}
	return false;
}

unsigned int CContainer::FindIDbyName(std::string searchName)
{
	if (!CheckHeader())
	{
		return false;
	}
	//Calculate pos for new entry
	m_fileObj.seekg(RADRESOURCECONTAINERHEADER_SIZE,std::ios::beg);
	m_fileObj.seekp(RADRESOURCECONTAINERHEADER_SIZE,std::ios::beg);
	if (m_header.fileCount>0)
	{
		unsigned int completeNameLen = 0;
		unsigned int completeSizeLen = 0;
		for(unsigned int ccc=0;ccc<m_header.fileCount;ccc++)
		{
			m_fileObj.seekg(RADRESOURCECONTAINERHEADER_SIZE+(RADRESOURCEHEADER_SIZE*ccc)+completeNameLen+completeSizeLen,std::ios::beg);
			m_fileObj.seekp(RADRESOURCECONTAINERHEADER_SIZE+(RADRESOURCEHEADER_SIZE*ccc)+completeNameLen+completeSizeLen,std::ios::beg);
			ResourceHeader m_headerSearch;
			if (m_fileObj.eof())
			{
				break;
			}
			//TODO: Replace with pointer reset
			m_fileObj.read(reinterpret_cast<char*>(&m_headerSearch.id), sizeof(m_headerSearch.id));
			m_fileObj.read(reinterpret_cast<char*>(&m_headerSearch.size), sizeof(m_headerSearch.size));
			m_fileObj.read(reinterpret_cast<char*>(&m_headerSearch.uncompressedSize), sizeof(m_headerSearch.uncompressedSize));
			m_fileObj.read(reinterpret_cast<char*>(&m_headerSearch.sizeName), sizeof(m_headerSearch.sizeName));
			std::string name(m_headerSearch.sizeName,'\0');
			completeNameLen += m_headerSearch.sizeName;
			for (unsigned char i=0;i<m_headerSearch.sizeName;i++)
			{
				m_fileObj >> name.at(i);
			}
			m_fileObj.seekg(1,std::ios::cur);
			m_fileObj.read(reinterpret_cast<char*>(&m_headerSearch.stat), sizeof(m_headerSearch.stat));
			m_fileObj.read(reinterpret_cast<char*>(&m_headerSearch.type), sizeof(m_headerSearch.type));
			m_fileObj.read(reinterpret_cast<char*>(&m_headerSearch.crc32), sizeof(m_headerSearch.crc32));
			completeSizeLen += m_headerSearch.size;
			if (name==searchName)
			{
				return m_headerSearch.id;
			}

			m_fileObj.seekg(m_headerSearch.size,std::ios::cur);
			m_fileObj.seekp(m_headerSearch.size,std::ios::cur);
		}
	}
	else
	{
		return -1;
	}
	return false;
}

bool CContainer::CheckHeader()
{
	if (!IsOpen())
	{
		if (m_enableStdout) {
		std::cout << "No container file open!" << std::endl;
		}
		return false;
	}
	m_fileObj.clear();
	m_fileObj.flush();
	m_fileObj.seekg(0,std::ios::beg);
	m_fileObj.seekp(0,std::ios::beg);
	m_fileObj.read(reinterpret_cast<char*>(&m_header.magic1), sizeof(m_header.magic1));
	m_fileObj.read(reinterpret_cast<char*>(&m_header.magic2), sizeof(m_header.magic2));
	m_fileObj.read(reinterpret_cast<char*>(&m_header.version), sizeof(m_header.version));
	m_fileObj.read(reinterpret_cast<char*>(&m_header.fileCount), sizeof(m_header.fileCount));
	m_fileObj.read(reinterpret_cast<char*>(&m_header.fileSize), sizeof(m_header.fileSize));
	{
		ContainerHeader m_headerCompare;
		if (!(m_header.magic1==m_headerCompare.magic1&&m_header.magic2==m_headerCompare.magic2&&m_header.version==m_headerCompare.version))
		{
			if (m_enableStdout) {
			std::cout << "Container is damaged or wrong version!" << std::endl;
			}
			return false;
		}
	}
if (m_enableStdout) {
	std::cout << "Size: " << m_header.fileSize << " Count:" << m_header.fileCount << std::endl;
}
	return true;
}

CResource::~CResource()
{
	if (m_data!=nullptr)
	{
		delete (char*)m_data;
	}
}

}
}
